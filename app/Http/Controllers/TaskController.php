<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Http\Resources\Task as TaskResource;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$tasks = Task::paginate( 10 );

		// retuan a collection of $tasks with pagination
		return TaskResource::collection( $tasks );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
		$task = $request->isMethod( 'put' ) ? Task::findOrFail( $request->task_id ) : new Task;
            
        $task->id = $request->input( 'task_id' );
        $task->name = $request->input( 'name' );
        $task->description = $request->input( 'description' );
        $task->user_id =  1; //$request->user()->id;
 
		if ( $task->save() )
		{
            return new TaskResource( $task) ;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
		$task = Task::findOrFail( $id );

		// returan a single task
		return new TaskResource( $task );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
		$task = Task::findOrFail( $id );

		if ($task->delete() )
		{
			return new TaskResource( $task );
		}
    }
}