<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post( '/login', 'LoginController@login' );
$router->post( '/register', 'UserController@register' );
$router->get( '/user', [ 'middleware' => 'auth', 'uses' =>  'UserController@get_user' ] );

// get list of tasks
Route::get( 'tasks', 'TaskController@index' );
// get specific task
Route::get( 'task/{id}', 'TaskController@show' );
// create new task
Route::post( 'task', 'TaskController@store' );
// update existing task
Route::put( 'task', 'TaskController@store' );
// delete a task
Route::delete( 'task/{id}', 'TaskController@destroy' );